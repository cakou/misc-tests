#!/bin/sh


echo "running linux kernel..."

KERNEL=$1
INITRD=$2

if [ "x$1" == "xhelp" ]; then
    echo "$0 [INITRD]"
    exit 0
fi

if [ "x$INITRD" = "x" ]; then
    CMDLINE="-kernel $KERNEL"
else
    CMDLINE="-kernel $KERNEL -initrd $INITRD"
fi

echo "cmd line : qemu $CMDLINE"

qemu $CMDLINE

