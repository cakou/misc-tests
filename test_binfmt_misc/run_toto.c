#include <stdio.h>

int main (int argc, char *argv[])
{
  FILE		*f;
  char		buf[64];
  int		i = 0;
  size_t	read;

  if (argc < 2)
    return 1;

  f = fopen(argv[1], "r");
  read = fread(buf, 1, 10, f);
  printf("file content:\n%.*s\n", read, buf);

  return 0;
}
