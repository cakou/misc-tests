/* Workers definitions */

#ifndef _WORKER_H
# define _WORKER_H



typedef struct s_winfo *t_winfo;

struct s_winfo
{
  t_queue		fdqueue;
  size_t		size;
  char		       *data;
  pthread_rwlock_t     *rwlock;
  pthread_barrier_t    *sync;
};

void   *worker			(void *arg);
t_winfo	build_info		(size_t nbw);

#endif
