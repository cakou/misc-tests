#include <stdio.h>
#include <pthread.h>
#include "queue.h"

# define NBTHREAD	30

typedef struct
{
  int			num;
  pthread_mutex_t	*mutex;
  pthread_cond_t	*cond;
  t_queue		*q;
}			t_args;


void	*eat(void	*a)
{
  t_args	*args = NULL;

  args = (t_args*)a;
  printf("Thread number %i\n", args->num);

  for (int i = 0; i < 40; i++)
    printf("Thread number %i: %i\n", args->num, take(*args->q));



  return (NULL);
}

int main (void)
{
  t_queue		q = NULL;
  t_args		args[NBTHREAD];
  pthread_t		threads[NBTHREAD];
  pthread_mutex_t	mutex = PTHREAD_MUTEX_INITIALIZER;
  pthread_cond_t	cond = PTHREAD_COND_INITIALIZER;

  for (int i = 0; i < NBTHREAD; i++)
    {
      args[i].num = i;
      args[i].q = &q;
      args[i].mutex = &mutex;
      args[i].cond = &cond;
    }

  q = new_queue();


  for (int i = 0; i < NBTHREAD; i++)
    pthread_create(&threads[i], NULL, eat, &args[i]);

  for (int i = 0; i < 30 * 1; i++)
    push(i, q);

  for (int i = 0; i < 30; i++)
    printf("%i\n", take(q));



  for (int i = 0; i < NBTHREAD; i++)
    pthread_join(threads[i], NULL);

  return (0);
}
