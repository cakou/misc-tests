#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>
#include <pthread.h>


#include "proto.h"
#include "queue.h"
#include "worker.h"

#define NBTH 16

/*
 * FUNCTIONS TO BE DONE:
 *  - launching threads pool
 */

void on_chld(int sig)
{
  wait(NULL);
  signal(sig,on_chld);
}

int fd_accept_reg(int fd)
{
  static int		saved;
  if (fd)
    saved = fd;
  return saved;
}

void on_int(int sig)
{
  sig = fd_accept_reg(0);
  close(sig);
  _exit(2);
}


int main()
{
  int			fd_accept;
  size_t		rlen;
  struct sockaddr_in	local;
  struct sockaddr_in	remote;
  t_winfo		info = NULL;
  pthread_t	       *tth = NULL;
  pthread_barrier_t	barrier;
  pthread_rwlock_t	rwlock;

  /* Initialization */
  memset (&local, 0, sizeof (local));
  local.sin_family = AF_INET;
  local.sin_addr.s_addr = htonl(INADDR_ANY);
  local.sin_port = htons(5555);
  /* Socket creation */
  fd_accept = socket (AF_INET, SOCK_STREAM, 0);
  if (fd_accept == -1)
    {
      perror("fd_accept error");
      exit(3);
    }
  fd_accept_reg(fd_accept);
  /* Caching signal */
  signal(SIGINT, on_int);
  /* Socket association */
  if (bind(fd_accept,(struct sockaddr*)&local, sizeof (local)) == -1)
    {
      perror("bind");
      close(fd_accept);
      exit(3);
    }
  /* Going to listen mode */
  listen (fd_accept, 5);
  /* Main loop */

  /* Before main loop we should launch threads pool */
  
  /*
  pthread_barrier_init(&barrier, NULL, NBTH);
  pthread_rwlock_init(&rwlock, NULL);
  info = malloc(sizeof (struct s_winfo));
  info->fdqueue = new_queue();
  info->size = NBTH;
  info->data = malloc(sizeof (char) * 64);
  info->rwlock = NULL;
  info->sync = &barrier;
  */
  tth = malloc(sizeof (pthread_t) * NBTH);

  info = build_info(NBTH);
  for (int i = 0; i < NBTH; i++)
    pthread_create(&tth[i], NULL, worker, (void*)info);

  for (;;)
    {
      int		fd;
      /* Initialisation de la structure remote */
      memset (&remote, 0, sizeof (remote));
      remote.sin_family = AF_INET ;
      rlen = sizeof (remote);
      fd = accept(fd_accept, (struct sockaddr*)&remote, (socklen_t*)&rlen);
      if (fd == -1)
	{
	  perror("Accepting connection");
	  continue;
	}
      printf("Connection accepted, fd=%i\n", fd);
      
      /* Push the new connexion so worker can treat it */
      push(fd, info->fdqueue);
    }
  return 0;
}
