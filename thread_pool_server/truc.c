#include <stdio.h>
#include <stdlib.h>

int main()
{
  printf("SET\r\n");
  for (size_t i = 0; i<65; ++i)
    printf("%c",(char)('a' + (random()%26)));
  printf("\r\n\r\n");
  return 0;
}
