/* Simple Protocol parsing */

#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include "proto.h"

int double_escape(char *buf, int len)
{
  return (len >= 4
	  && buf[len-4] == '\r'
	  && buf[len-3] == '\n'
	  && buf[len-2] == '\r'
	  && buf[len-1] == '\n');
}

int full_read(int fd, char buf[], size_t l)
{
  int			r;
  size_t		pos=0;
  while ((r = read(fd, buf+pos, l - pos)))
    {
      if (r == -1)
	{
	  if (errno == EINTR)
	    continue;
	  else
	    {
	      perror("parse");
	      abort();
	    }
	}
      if ((pos += r) >= l || double_escape(buf, pos))
	break;
    }
  return (pos);
}

t_query make_q(int set)
{
  t_query		q;
  q = malloc(sizeof (struct s_query));
  if (set)
    q->qtype = SET;
  else
    {
      q->qtype = GET;
      q->data = NULL;
      q->size = 0;
    }
  return q;
}

size_t trim(char buf[], size_t len)
{
  if (buf[len - 1] == '\r')
    {
      buf[len - 1] = 0;
      return trim(buf, len-1);
    }
  if (buf[len - 1] == '\n' && buf[len - 2] == '\r')
    {
      buf[len - 1] = 0;
      buf[len - 2] = 0;
      return trim(buf, len-2);
    }
  return len;
}

#define DATA_SIZE 64

size_t read_data(int fd, char *ret[])
{
  char		       *buf, discard[64];
  int			len;

  // We're only interested in the first 64 bytes
  buf = malloc(DATA_SIZE + 4);
  if ((len = full_read(fd,buf,DATA_SIZE + 4)))
    {
      // suppress end sequence
      if (len>64)
	buf = realloc(buf,64);
      else
	len = trim(buf,len);
      // discard the rest of the input stream
      //while (full_read(fd,discard,64)>=64) ;
      // FIXME: ask to marwan...
      *ret = buf;
      return ((len<64) ? len : 64);
    }
  *ret = NULL;
  return 0;
}

t_query parse_query(int fd)
{
  t_query		q = NULL;
  char			buf[5];
  // First read the command
  if (full_read(fd,buf,5))
    {
      if (strncmp(buf,"SET\r\n",5) == 0)
	{
	  q = make_q(1);
	  q->size = read_data(fd, &(q->data));
	}
      else
	{
	  if (strncmp(buf,"GET\r\n",5) == 0)
	    q = make_q(0);
	  else
	    return NULL;
	}
    }
  return q;
}

void free_query(t_query q)
{
  switch (q->qtype)
    {
    case GET: free(q); break;
    case SET:
      {
	free(q->data);
	free(q);
      }
    }
}

void print_q(t_query q)
{
  switch (q->qtype)
    {
    case GET: printf("{GET}\n"); break;
    case SET:
      {
	char		b[65];
	strncpy(b,q->data,q->size);
	b[q->size] = 0;
	printf("{SET: %s (%u)}\n",b, q->size);
      }
    }
}
