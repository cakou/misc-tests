#include "queue.h"

pthread_mutex_t	mutex;
pthread_cond_t	cond;

t_queue	new_queue	(void)
{
  t_queue	newq = NULL;

  newq = malloc(sizeof (struct s_cq));
  newq->tail = NULL;
  newq->head = NULL;
  pthread_mutex_init(&mutex, NULL);
  pthread_cond_init(&cond, NULL);
  return (newq);
}

void	push		(int x, t_queue q)
{
  struct list	*l = NULL;

  l = malloc(sizeof (struct list));
  l->elt = x;
  l->next = NULL;

  pthread_mutex_lock(&mutex);
  // critical section
  if (q->head == NULL)
  {
    q->head = l;
    q->tail = l;
  }
  else
  {
    q->tail->next = l;
    q->tail = l;
  }
  pthread_mutex_unlock(&mutex);
  while (pthread_cond_broadcast(&cond));	// others thread can take.
}

int	take		(t_queue q)
{
  int		res = 0;
  struct list	*tmp;

  pthread_mutex_lock(&mutex);

  while ((q->head == NULL) || (q->tail == NULL))
    pthread_cond_wait(&cond, &mutex);

  tmp = q->head;
  res = tmp->elt;
  if (q->head == q->tail)
  {
      q->head = NULL;
      q->tail = NULL;
  }
  else
    q->head = tmp->next;

  pthread_mutex_unlock(&mutex);
  free(tmp);
  return (res);
}

void    clear_queue	(t_queue q)
{
  while (q)
    take(q);
}

