#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>
#include <stdio.h>
#include <poll.h>

#include "proto.h"
#include "queue.h"
#include "worker.h"

/* sending data function */
void send_data(int fd, char buf[], size_t size)
{
  int			s;
  size_t		pos=0;
  while (pos<size)
    {
      s = write(fd, buf+pos, size - pos);
      if (s == -1)
	{
	  if (errno == EINTR)
	    continue;
	  perror("send_data <FATAL ERROR>");
	  abort();
	}
      pos += s;
    }
}

/* bounded time waiting for close */
void wait_close(int fd)
{
  struct pollfd		pfd;
  pfd.fd = fd;
  pfd.events = POLLHUP;
  if (poll(&pfd,1,5000) < 1 || pfd.revents & POLLHUP)
    close(fd);
}

void handle(int fd, t_query q, t_winfo info)
{
  switch (q->qtype)
    {
    case GET:
      {
	/* handle get task */
	pthread_rwlock_rdlock(info->rwlock);
	printf("Data read is %s (%i bytes):\n", info->data, info->size);
	send_data(fd, info->data, info->size);
	send_data(fd, "\r\n", 2);
	pthread_rwlock_unlock(info->rwlock);

	break;
      }
    case SET:
      {
	/* handle set task */
	pthread_rwlock_wrlock(info->rwlock);
	memcpy(info->data, q->data, q->size);
	info->size = q->size;
	printf("Data is now %s (%i bytes):\n", info->data, q->size);
	pthread_rwlock_unlock(info->rwlock);
	send_data(fd, "OK\r\n", 4);
      }
    }
  wait_close(fd);
}

void *worker(void *arg)
{
  t_winfo		info = NULL;
  t_query		query = NULL;
  int			fd = 0;

  info = arg;

  /* First wait for all workers to be up */
  pthread_barrier_wait(info->sync);
  printf("Start of thread %i\n", (int)pthread_self());
  
  for (;;)
    {
      /* Wait for task, and handle it */
      fd = take(info->fdqueue);
      printf("Taked on %i: %i\n", (int)pthread_self(), fd);
      query = parse_query(fd);
      print_q(query);
      handle(fd, query, info);
      free_query(query);
      close(fd);
    }

  pthread_exit(NULL);
}


/* building workers' info block */
t_winfo build_info(size_t nbw)
{
  t_winfo		info;
  info = malloc(sizeof (struct s_winfo));
  info->fdqueue = new_queue();
  info->size = 18;
  info->data = calloc(64,1);
  memcpy(info->data, "-- INITIAL DATA --", 18);
  info->rwlock = malloc(sizeof (pthread_rwlock_t));
  info->sync = malloc(sizeof (pthread_barrier_t));
  pthread_barrier_init(info->sync, NULL, nbw);
  pthread_rwlock_init(info->rwlock, NULL);
  return info;
}

