/* Concurrent Queue */

#ifndef _QUEUUE_H
# define _QUEUUE_H

# include <pthread.h>
# include <stdlib.h>

struct list
{
  int		elt;
  struct list	*next;
};


struct s_cq
{
  struct list	*head;
  struct list	*tail;
};

typedef struct s_cq    *t_queue;

t_queue	new_queue	(void);
void	push		(int		x, t_queue	q);
int	take		(t_queue	q);
void    clear_queue	(t_queue	q);

#endif
