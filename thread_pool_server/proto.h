/* Simple Protocol parsing */

#include <stdlib.h>
#include <unistd.h>

#ifndef _PROTO_H
#define _PROTO_H

enum e_qtype {GET, SET};

struct s_query
{
  enum e_qtype		qtype;
  char		       *data;
  size_t		size;
};

typedef struct s_query *t_query;

t_query	parse_query	(int fd);
void	free_query	(t_query q);
void	print_q		(t_query q);

#endif
